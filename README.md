***CardBook*** – *a new Thunderbird address book based on the CardDAV and vCard standards*

--------

### Features

* **Autocompletion** in mail address fields and [Lightning](https://addons.thunderbird.net/addon/lightning/) calendar fields
* Manage contacts from messages
* Restrict address book use by mail account (for email autocompletion, email collection and contact registration)
* Easy **[CardDAV](https://en.wikipedia.org/wiki/CardDAV) synchronization**
* Access to **all [vCard](https://en.wikipedia.org/wiki/VCard) data**
* Supports [vCard 3.0](https://en.wikipedia.org/wiki/VCard#vCard_3.0) and [vCard 4.0](https://en.wikipedia.org/wiki/VCard#vCard_4.0) (default)
* **Customizable** data fields
* Unlimited number of custom fields
* Address panel
* Show address on map
* Phone number validation
* Integrated phone calls with [SIP](https://en.wikipedia.org/wiki/Session_Initiation_Protocol)
* [Digest access authentication](https://en.wikipedia.org/wiki/Digest_access_authentication)
* [Drag and drop](https://en.wikipedia.org/wiki/Drag_and_drop) support
* [Saved search](https://en.wikipedia.org/wiki/Saved_search)
* Filter
* Duplicate and merge contacts
* Share contacts by email
* Contacts stored in [IndexedDB](https://en.wikipedia.org/wiki/Indexed_Database_API)
* Customizable printouts
* [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) file export and import
* Functional [GUI](https://en.wikipedia.org/wiki/Graphical_user_interface)
* Classic and vertical layout
* Anniversary manager
* Task manager
* Works with 
[Cozy](https://cozy.io/), 
[Google](https://www.google.com/), 
[iCloud](https://www.icloud.com/), 
[Memotoo](https://www.memotoo.com/), 
[Nextcloud](https://nextcloud.com/), 
[ownCloud](https://owncloud.org/), 
[Yahoo!](https://login.yahoo.com), 
[Zimbra](https://www.zimbra.com/), 
and more
* Available in many [languages](https://gitlab.com/CardBook/CardBook#translators)
* Supported operating systems 
[Apple macOS](https://en.wikipedia.org/wiki/MacOS), 
[BSD](https://en.wikipedia.org/wiki/Berkeley_Software_Distribution), 
[Linux](https://en.wikipedia.org/wiki/Linux), 
[Microsoft Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)
* [Free/Libre Open Source Software (FLOSS)](https://www.gnu.org/philosophy/floss-and-foss.en.html)
* *And many more…*


### Installation

You can get CardBook from the [official Thunderbird add-on page](https://addons.thunderbird.net/addon/cardbook/)!

Once CardBook is installed, you can also run CardBook as a standalone program:<br>
Windows `thunderbird.exe -cardbook`<br>
OSX `thunderbird -cardbook`<br>
Linux `thunderbird -cardbook`

### Compatible and complementary add-ons
* [Mail Merge](https://addons.thunderbird.net/addon/mail-merge/)
* [Mail Redirect](https://addons.thunderbird.net/addon/mailredirect/)
* [TbSync (Provider for Exchange ActiveSync)](https://addons.thunderbird.net/addon/tbsync/)


### Issues

If you encounter any problems with CardBook please have a look at our [GitLab issue tracker](https://gitlab.com/CardBook/CardBook/issues) and [our forum](https://cardbook.6660.eu/).
If your problem is not listed there, you can do us a great favor by creating a new issue. But even if there is already an issue discussing the topic, you could help us by providing additional information in a comment.

When you are creating an issue, please give the following information:
* a detailed description of the problem
* the situation the problem occurs in and how other people will be able to recreate it, i.e. the steps to reproduce the problem
* your Thunderbird version (you can find this in the main menu at `Help → About Thunderbird`)
* your CardBook version (you can find this in the CardBook Preferences window [title bar](https://en.wikipedia.org/wiki/Window_decoration#Title_bar) `CardBook → Preferences`)
* your operating system and version
* if possible: the **relevant** output of the error console (found at `Tools → Developer Tools → Error Console` / `Ctrl-Shift-J`)


### Roadmap

Planned features:
* Better integration with the standard Thunderbird address book

Community efforts:
* A project wiki (in [GitLab](https://gitlab.com/CardBook/CardBook/wikis/home))


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations (i.e. translations) at [locale](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale) or by emailing (in English or French) [cardbook.thunderbird@gmail.com](mailto:cardbook.thunderbird@gmail.com),
* creating [issues](https://gitlab.com/CardBook/CardBook/issues) about problems,
* creating [issues](https://gitlab.com/CardBook/CardBook/issues) about possible improvements,
* helping people who have problems or questions,
* improving the documentation,
* working on the code ([Mozilla Coding Style Guide](https://developer.mozilla.org/docs/Mozilla_Coding_Style_Guide)),
* or simply spreading the word about this great add-on.

### Coders
* Philippe Vigneau (author and maintainer)
* [Alexander Bergmann](https://addons.thunderbird.net/user/tempuser/)
* [John Bieling](https://github.com/jobisoft)
* Sisim Biva
* [Günter Gersdorf](https://addons.thunderbird.net/user/guenter-gersdorf/)
* [R. Kent James](https://github.com/rkent)
* Axel Liedtke
* Timothe Litt
* Christoph Mair
* Günther Palfinger
* Boris Prüssmann
* Michael Roland
* Lukáš Tyrychtr
* Marco Zehe

### Translators
* Lukáš Tyrychtr ([cs](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/cs)) [Stáhnout](https://addons.thunderbird.net/cs/addon/cardbook/)
* Johnny Nielsen ([da](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/da)) [Hent nu](https://addons.thunderbird.net/da/addon/cardbook/)
* Alexander Bergmann ([de](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/de)) [Jetzt herunterladen](https://addons.thunderbird.net/de/addon/cardbook/)
* Markus Mauerer ([de](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/de)) [Jetzt herunterladen](https://addons.thunderbird.net/de/addon/cardbook/)
* Oliver Schuppe ([de](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/de)) [Jetzt herunterladen](https://addons.thunderbird.net/de/addon/cardbook/)
* SusiTux ([de](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/de)) [Jetzt herunterladen](https://addons.thunderbird.net/de/addon/cardbook/)
* Γιώτα ([el](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/el)) [Λήψη τώρα](https://addons.thunderbird.net/el/addon/cardbook/)
* Μαρία ([el](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/el)) [Λήψη τώρα](https://addons.thunderbird.net/el/addon/cardbook/)
* Timothe Litt ([en-US](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/en-US)) [Download Now](https://addons.thunderbird.net/en-US/addon/cardbook/)
* Óvári ([en-US](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/en-US)) [Download Now](https://addons.thunderbird.net/en-US/addon/cardbook/)
* Sylvain Lesage ([es-ES](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/es-ES)) [Descargar ahora](https://addons.thunderbird.net/es-ES/addon/cardbook/)
* Philippe Vigneau ([fr](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/fr)) [Télécharger maintenant](https://addons.thunderbird.net/fr/addon/cardbook/)
* Meri ([hr](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/hr)) [Preuzeti sada](https://addons.thunderbird.net/hr/thunderbird/addon/cardbook/)
* Óvári ([hu](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/hu)) [Letöltés most](https://addons.thunderbird.net/hu/addon/cardbook/)
* Zuraida ([id](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/id)) [Unduh Sekarang](https://addons.thunderbird.net/id/addon/cardbook/)
* Stefano Bloisi ([it](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/it)) [Scarica ora](https://addons.thunderbird.net/it/addon/cardbook/)
* Agnese Morettini ([it](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/it)) [Scarica ora](https://addons.thunderbird.net/it/addon/cardbook/)
* いつき ([ja](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ja)) [今すぐダウンロード](https://addons.thunderbird.net/ja/addon/cardbook/)
* 千秋 ([ja](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ja)) [今すぐダウンロード](https://addons.thunderbird.net/ja/addon/cardbook/)
* 공성은 ([ko](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ko)) [다운로드](https://addons.thunderbird.net/ko/addon/cardbook/)
* Gintautas ([lt](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/lt)) [Parsiųsti dabar](https://addons.thunderbird.net/lt/thunderbird/addon/cardbook/)
* Han Knols ([nl](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/nl)) [Nu downloaden](https://addons.thunderbird.net/nl/addon/cardbook/)
* Adam Gorzkiewicz ([pl](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/pl)) [Pobierz](https://addons.thunderbird.net/pl/addon/cardbook/)
* Dominik Wnęk ([pl](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/pl)) [Pobierz](https://addons.thunderbird.net/pl/addon/cardbook/)
* [André Bação](https://github.com/abacao) ([pt-PT](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/pt-PT)) [Transferir agora](https://addons.thunderbird.net/pt-PT/addon/cardbook/)
* Lucian Burca ([ro](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ro)) [Descarcă acum](https://addons.thunderbird.net/ro/addon/cardbook/)
* Florin Craciunica ([ro](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ro)) [Descarcă acum](https://addons.thunderbird.net/ro/addon/cardbook/)
* Alexander Yavorsky ([ru](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/ru)) [Загрузить сейчас](https://addons.thunderbird.net/ru/addon/cardbook/)
* Peter Klofutar ([sl](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/sl)) [Prenesi zdaj](https://addons.thunderbird.net/sl/addon/cardbook/)
* Anders ([sv-SE](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/sv-SE)) [Hämta nu](https://addons.thunderbird.net/sv-SE/addon/cardbook/)
* Kire Dyfvelsten ([sv-SE](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/sv-SE)) [Hämta nu](https://addons.thunderbird.net/sv-SE/addon/cardbook/)
* Валентина ([uk](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/uk)) [Завантажити зараз](https://addons.thunderbird.net/uk/addon/cardbook/)
* Христина ([uk](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/uk)) [Завантажити зараз](https://addons.thunderbird.net/uk/addon/cardbook/)
* Loan ([vi](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/vi)) [Tải xuống ngay](https://addons.thunderbird.net/vi/addon/cardbook/)
* Nguyễn ([vi](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/vi)) [Tải xuống ngay](https://addons.thunderbird.net/vi/addon/cardbook/)
* Thanh ([vi](https://gitlab.com/CardBook/CardBook/tree/master/chrome/locale/vi)) [Tải xuống ngay](https://addons.thunderbird.net/vi/addon/cardbook/)
* [Transvision](https://transvision.mozfr.org/)
* [KDE Localization](https://l10n.kde.org/dictionary/search-translations.php)


#### Trailblazer
* [Thomas McWork](https://github.com/thomas-mc-work): midwife for the public git repository

### License

[Mozilla Public License version 2.0](https://gitlab.com/CardBook/CardBook/blob/master/LICENSE.txt)

if ("undefined" == typeof(wdw_addressbooksEdit)) {
	try {
		ChromeUtils.import("chrome://cardbook/content/cardbookRepository.js");
	}
	catch(e) {
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
	}

	var wdw_addressbooksEdit = {
		
		initialDateFormat: "",

		getAllTypesByType: function (aType) {
			try {
				var count = {};
				var finalResult = [];
				var finalResult1 = [];
				if (aType === "adr" || aType === "address") {
					var result = Services.prefs.getChildList("extensions.cardbook.types." + "adr" + ".", count);
					if (result.length == 0) {
						var result = Services.prefs.getChildList("extensions.cardbook.types." + "address" + ".", count);
					}
				} else {
					var result = Services.prefs.getChildList("extensions.cardbook.types." + aType + ".", count);
				}
				
				for (let i = 0; i < result.length; i++) {
					finalResult.push(cardbookPreferences.getStringPref(result[i]));
				}
				finalResult = cardbookRepository.arrayUnique(finalResult);
				for (let i = 0; i < finalResult.length; i++) {
					if (finalResult[i].indexOf(":") > 0) {
						var tmpArray = finalResult[i].split(":");
						if (tmpArray[1] != null && tmpArray[1] !== undefined && tmpArray[1] != "") {
							finalResult1.push([tmpArray[0], tmpArray[1]]);
						} else {
							finalResult1.push([tmpArray[0], tmpArray[0]]);
						}
					} else {
						try {
							var strBundle = Services.strings.createBundle("chrome://cardbook/locale/cardbook.properties");
							var translated = strBundle.GetStringFromName("types." + aType.toLowerCase() + "." + finalResult[i].toLowerCase());
							if (translated != null && translated !== undefined && translated != "") {
								finalResult1.push([finalResult[i], translated]);
							} else {
								finalResult1.push([finalResult[i], finalResult[i]]);
							}
						}
						catch(e) {
							finalResult1.push([finalResult[i], finalResult[i]]);
						}
					}
				}
				finalResult1 = cardbookUtils.sortArrayByString(finalResult1,1,1);
				return finalResult1;
			}
			catch(e) {
				Services.console.logStringMessage("wdw_addressbooksEdit.getAllTypesByType error : " + e + "\n");
			}
		},

		convertOldCustomTypes: function () {
			var typesList = [ 'email', 'tel', 'url', 'adr' ];
			var ABType = cardbookPreferences.getType(window.arguments[0].dirPrefId);
			var ABTypeFormat = cardbookRepository.getABTypeFormat(ABType);

			var myRemainingTypes = {};
			for (var z in typesList) {
				var myType = typesList[z];
				var myPrefsTypes = wdw_addressbooksEdit.getAllTypesByType(myType);
				for (var l = 0; l < myPrefsTypes.length; l++) {
					var myPrefCode = myPrefsTypes[l][0];
					var found = false;
					for (var i = 0; i < cardbookRepository.cardbookCoreTypes[ABTypeFormat][myType].length && !found; i++) {
						var types = cardbookRepository.cardbookCoreTypes[ABTypeFormat][myType][i][1];
						var possibilities = types.split(";");
						for (var j = 0; j < possibilities.length; j++) {
							var possibility = possibilities[j].split(",");
							if (possibility.indexOf(myPrefCode.toUpperCase()) != -1) {
								found = true;
								break;
							}
						}
					}
					if (!found) {
						if (!myRemainingTypes[myType]) {
							myRemainingTypes[myType] = [];
						}
						myRemainingTypes[myType].push(myPrefsTypes[l]);
					}
				}
			}
			for (i in cardbookRepository.cardbookCards) {
				var myCard = cardbookRepository.cardbookCards[i];
				if (myCard.dirPrefId != window.arguments[0].dirPrefId) {
					continue;
				}
				var myTempCard = new cardbookCardParser();
				cardbookUtils.cloneCard(myCard, myTempCard);
				var cardChanged = false;
				for (var j in typesList) {
					var myType = typesList[j];
					if (!(myRemainingTypes[myType])) {
						continue;
					}
					for (var k = 0; k < myCard[myType].length; k++) {
						var myCardTypes = myCard[myType][k][1];
						var myInputTypes = cardbookUtils.getOnlyTypesFromTypes(myCardTypes);
						for (var l = 0; l < myRemainingTypes[myType].length; l++) {
							if (myInputTypes.indexOf(myRemainingTypes[myType][l][0].toUpperCase()) != -1) {
								var myPref = cardbookUtils.getPrefBooleanFromTypes(myCardTypes)
								var myPrefValue = cardbookUtils.getPrefValueFromTypes(myCardTypes, myCard.version)
								var lPrefString = "";
								if (myCard.version == "4.0") {
									if (myPrefValue != "") {
										lPrefString = "PREF=" + myPrefValue;
									} else {
										lPrefString = "PREF";
									}
								} else {
									lPrefString = "TYPE=PREF";
								}
								myTempCard[myType][k] = [ myCard[myType][k][0], [lPrefString] , "ITEM1", [myRemainingTypes[myType][l][1]] ];
								cardChanged = true;
								break;
							}
						}
					}
				}
				if (cardChanged) {
					cardbookTypes.rebuildAllPGs(myTempCard);
					cardbookRepository.saveCard(myCard, myTempCard, "cardbook.cardAddedDirect");
				} else {
					myTempCard = null;
				}
			}
		},

		loadFnFormula: function () {
			document.getElementById("fnFormulaTextBox").value = cardbookPreferences.getFnFormula(window.arguments[0].dirPrefId);
			var strBundle = document.getElementById("cardbook-strings");
			var orgStructure = cardbookPreferences.getStringPref("extensions.cardbook.orgStructure");
			if (orgStructure != "") {
				var allOrg = cardbookUtils.unescapeArray(cardbookUtils.escapeString(orgStructure).split(";"));
			} else {
				var allOrg = [];
			}
			var myLabel = "";
			myLabel = myLabel + "{{1}} : " + strBundle.getString("prefixnameLabel") + "    ";
			myLabel = myLabel + "{{2}} : " + strBundle.getString("firstnameLabel") + "    ";
			myLabel = myLabel + "{{3}} : " + strBundle.getString("othernameLabel") + "    ";
			myLabel = myLabel + "{{4}} : " + strBundle.getString("lastnameLabel");
			document.getElementById('fnFormulaDescriptionLabel1').value = myLabel.trim();
			myLabel = "";
			myLabel = myLabel + "{{5}} : " + strBundle.getString("suffixnameLabel") + "    ";
			myLabel = myLabel + "{{6}} : " + strBundle.getString("nicknameLabel") + "    ";
			var count = 7;
			if (allOrg.length === 0) {
				myLabel = myLabel + "{{" + count + "}} : " + strBundle.getString("orgLabel");
				count++;
			} else {
				for (var i = 0; i < allOrg.length; i++) {
					myLabel = myLabel + "{{" + count + "}} : " + allOrg[i] + "    ";
					count++;
				}
			}
			document.getElementById('fnFormulaDescriptionLabel2').value = myLabel.trim();
			myLabel = "";
			myLabel = myLabel + "{{" + count + "}} : " + strBundle.getString("titleLabel") + "    ";
			count++;
			myLabel = myLabel + "{{" + count + "}} : " + strBundle.getString("roleLabel") + "    ";
			document.getElementById('fnFormulaDescriptionLabel3').value = myLabel.trim();
		},

		resetFnFormula: function () {
			document.getElementById('fnFormulaTextBox').value = cardbookRepository.defaultFnFormula;
		},

		showAutoSyncInterval: function () {
			if (document.getElementById('autoSyncCheckBox').checked) {
				document.getElementById('autoSyncInterval').disabled = false;
				document.getElementById('autoSyncIntervalTextBox').disabled = false;
			} else {
				document.getElementById('autoSyncInterval').disabled = true;
				document.getElementById('autoSyncIntervalTextBox').disabled = true;
			}
		},

		load: function () {
			wdw_addressbooksEdit.initialDateFormat = cardbookPreferences.getDateFormat(window.arguments[0].dirPrefId);

			document.getElementById("colorInput").value = cardbookPreferences.getColor(window.arguments[0].dirPrefId);
			document.getElementById("nameTextBox").value = cardbookPreferences.getName(window.arguments[0].dirPrefId);
			document.getElementById("typeTextBox").value = cardbookPreferences.getType(window.arguments[0].dirPrefId);
			document.getElementById("urlTextBox").value = cardbookPreferences.getUrl(window.arguments[0].dirPrefId);
			document.getElementById("usernameTextBox").value = cardbookPreferences.getUser(window.arguments[0].dirPrefId);
			document.getElementById("readonlyCheckBox").setAttribute('checked', cardbookPreferences.getReadOnly(window.arguments[0].dirPrefId));
			document.getElementById("vCardVersionTextBox").value = cardbookPreferences.getVCardVersion(window.arguments[0].dirPrefId);
			cardbookElementTools.loadDateFormats("dateFormatMenuPopup", "dateFormatMenuList", wdw_addressbooksEdit.initialDateFormat);
			document.getElementById("urnuuidCheckBox").setAttribute('checked', cardbookPreferences.getUrnuuid(window.arguments[0].dirPrefId));

			if (document.getElementById("typeTextBox").value == "GOOGLE" || document.getElementById("typeTextBox").value == "APPLE"
					|| document.getElementById("typeTextBox").value == "CARDDAV" || document.getElementById("typeTextBox").value == "YAHOO") {
				document.getElementById('syncTab').setAttribute("collapsed", false);
				document.getElementById("autoSyncCheckBox").setAttribute('checked', cardbookPreferences.getAutoSyncEnabled(window.arguments[0].dirPrefId));
				document.getElementById("autoSyncIntervalTextBox").value = cardbookPreferences.getAutoSyncInterval(window.arguments[0].dirPrefId);
				wdw_addressbooksEdit.showAutoSyncInterval();
			} else {
				document.getElementById('syncTab').setAttribute("collapsed", true);
			}
			
			if (cardbookPreferences.getBoolPref("extensions.cardbook.convertOldCustomTypes")) {
				document.getElementById('convertOldCustomTypesButton').removeAttribute('hidden');
			} else {
				document.getElementById('convertOldCustomTypesButton').setAttribute('hidden', 'true');
			}
			
			wdw_addressbooksEdit.loadFnFormula();
		},

		save: function () {
			if (document.getElementById('dateFormatMenuList').value != wdw_addressbooksEdit.initialDateFormat) {
				cardbookDates.convertAddressBookDate(window.arguments[0].dirPrefId, document.getElementById('nameTextBox').value,
														wdw_addressbooksEdit.initialDateFormat, document.getElementById('dateFormatMenuList').value);
			}
			cardbookPreferences.setName(window.arguments[0].dirPrefId, document.getElementById('nameTextBox').value);
			cardbookPreferences.setColor(window.arguments[0].dirPrefId, document.getElementById('colorInput').value);
			cardbookPreferences.setVCardVersion(window.arguments[0].dirPrefId, document.getElementById('vCardVersionTextBox').value);
			cardbookPreferences.setReadOnly(window.arguments[0].dirPrefId, document.getElementById('readonlyCheckBox').checked);
			cardbookPreferences.setDateFormat(window.arguments[0].dirPrefId, document.getElementById('dateFormatMenuList').value);
			cardbookPreferences.setUrnuuid(window.arguments[0].dirPrefId, document.getElementById('urnuuidCheckBox').checked);
			cardbookPreferences.setAutoSyncEnabled(window.arguments[0].dirPrefId, document.getElementById('autoSyncCheckBox').checked);
			cardbookPreferences.setAutoSyncInterval(window.arguments[0].dirPrefId, document.getElementById('autoSyncIntervalTextBox').value);
			cardbookPreferences.setFnFormula(window.arguments[0].dirPrefId, document.getElementById('fnFormulaTextBox').value);
			
			if (document.getElementById('autoSyncCheckBox').checked) {
				if (!(cardbookRepository.autoSyncId[window.arguments[0].dirPrefId] != null && cardbookRepository.autoSyncId[window.arguments[0].dirPrefId] !== undefined && cardbookRepository.autoSyncId[window.arguments[0].dirPrefId] != "")) {
					cardbookSynchronization.addPeriodicSync(window.arguments[0].dirPrefId, document.getElementById('nameTextBox').value, document.getElementById('autoSyncIntervalTextBox').value);
				}
			} else {
				if (cardbookRepository.autoSyncId[window.arguments[0].dirPrefId] != null && cardbookRepository.autoSyncId[window.arguments[0].dirPrefId] !== undefined && cardbookRepository.autoSyncId[window.arguments[0].dirPrefId] != "") {
					cardbookSynchronization.removePeriodicSync(window.arguments[0].dirPrefId, document.getElementById('nameTextBox').value);
				}
			}
			
			window.arguments[0].serverCallback("SAVE", window.arguments[0].dirPrefId, document.getElementById('nameTextBox').value,
												document.getElementById('readonlyCheckBox').checked);
			close();
		},

		cancel: function () {
			window.arguments[0].serverCallback("CANCEL", window.arguments[0].dirPrefId);
			close();
		}

	};

};
